import controlP5.*;

/*
*  \name:  filtros
*  \brief:ejemplo de diferentes filtros para el curso de introduccion en Semanai
*  \date: 26/09/17
*  \autor: Oscar Hinojosa
*/

/*Declaracion de variables globales de este skech*/
ControlP5 cp5;
Button B1,B2;
Slider s1;
PImage img,img2,img3;
int state=0;
int limite=100;
Textlabel l1;
void setup()
{
 size(1080,920); 
  cp5 = new ControlP5(this);/* creacion de constructor*/
  img =loadImage("mhu.jpg");
  img2 =loadImage("mhu2.jpg");
  img3 = new PImage (img2.width, img2.height, RGB);
  s1= cp5.addSlider("Porcentaje")
   .setPosition(500,890)
   .setSize(150,20)/*tamaño del slider*//*Ubicacion del slider*/
   .setRange(0,100)/*Asignas el rango de movimiento*/
   .setValue(50)/*valor de inicio*/
   .setNumberOfTickMarks(101);   /*fatla una configuracion*/

  B1 =cp5.addButton("Next")
     //.setValue(0)
     .setPosition(10,890)
     .setSize(200,20)
     ;
     B2 =cp5.addButton("Before")
     //.setValue(0)
     .setPosition(870,890)
     .setSize(200,20)
     ;
   cp5.addLabel("Filtro",350,890);
  l1= cp5.addLabel("0000000",400,890).setText("hola");
}

void draw()
{
  img =loadImage("mhu.jpg");
  background(0);
  noStroke();
   img.loadPixels();
 

   for(int i =0; i < img.width;i++)
 {
   for(int j = 0; j < img.height ; j++)
   {
     int k= i + (j*img.width); /*esto es para rellenar el arreglo de pixels con una matriz*/
        switch(state){
           case 1:
           FiltroGrises(k);
           break;
           case 2:
           BalckandWhite(k);
           break;
           case 3:
           Negativo(k);
           break;
           case 4:
           Diferencia(k);
           break;
        }
     }
  }
   
  img.updatePixels();
 
 image(img,10,50);

  
}

public void Next(int valor)
{
  println("boton next presionado:"+valor);
    println(state);
  if(4 == state)
  {
    state=0;
    l1.setText("original");

  }
  else
  {
  state++;
  }
}

public void Before(int valor)
{
  println("boton before presionado:"+valor);
  println(state);
  if(0 == state)
  {
    state=4;
      l1.setText("original");

  }
  else
  {
  state--;
  }
}

void FiltroGrises(int k)
{
  l1.setText("Grises");
        float vActual   = green (img.pixels[k]) ,
                rActual  = red  (img.pixels[k]) ,
                aACtual  = blue (img.pixels[k]) ,
                promedio = ((rActual+vActual+aACtual) / 3);
            
              img.pixels[k]= color(promedio,promedio,promedio);
}
void BalckandWhite(int k)
{
  l1.setText("B&W");
  float vActual   = green (img.pixels[k]) ,
                rActual  = red  (img.pixels[k]) ,
                aACtual  = blue (img.pixels[k]) ,
                promedio = ((rActual+vActual+aACtual) / 3);
            
              
              if(promedio > limite)
              {
                img.pixels[k]= color(0,0,0);
              }
              else
              {
                 img.pixels[k]= color(255,255,255);
              }
}
void Negativo(int k)
{
  l1.setText("Negativo");
  float vActual  = green (img.pixels[k]) ,
        rActual  = red  (img.pixels[k]) ,
        aACtual  = blue (img.pixels[k]) ;
  img.pixels[k]= color ((255-rActual),(255-vActual),(255-aACtual));
}

void Diferencia(int k)
{
  l1.setText("Diferencias");
  float vActual   = green (img.pixels[k]) ,
                rActual  = red  (img.pixels[k]) ,
                aACtual  = blue (img.pixels[k]) ,
                promedio = ((rActual+vActual+aACtual) / 3);
 float vActual2   = green (img2.pixels[k]) ,
                rActual2  = red  (img2.pixels[k]) ,
                aACtual2  = blue (img2.pixels[k]) ,
                promedio2 = ((rActual2+vActual2+aACtual2) / 3);
  if( limite > (abs(promedio-promedio2)))
  {
    img.pixels[k] = color(0,0,0);
  }
}
void Porcentaje(float r)
{
 println("Porcentaje: " + r);
 limite=int(r);
}